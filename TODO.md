###TODO LIST for functionality

* navbar must be configurable according to the user and group data
* navbar options must be shown/hidden according to current user roles
* refer to layouts/application_events.erb in SHORTPATH for this info

* Adding events should have all the fields entered and constantly checking for validation
* event must have a title
* error box should show on top of the respective errors needed to fix
* date cannot be before today
* time cannot be before now and closest to current time to half hour - increment by 30 minutes each
* duration defaults to 1 hour - increment by 1 hour each to 10 hours
* repeat options: weekly, monthly, years
  * weekly makes you pick days of week and then end date
  * monthly makes you pick end month
  * years makes you pick end year
* location is grabbed from API and must is required
* description can be toggled on and off as a text field box
* guests can be uploaded via CSV
* guests can be searched with an autocomplete from the system (otherwise, creates new one with name, company, and email - only name is required)
* organizers can be searched with an autocomplete from the system (otherwise added as well?)
* organizers can be checkboxed to recieve email notifications or not

* add event send the final ajax request and then prompt to email organizers now or not

* constantly making AJAX requests on event change