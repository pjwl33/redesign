window.AppService = {
    fetchAppData: function(options, success, error) {
        if (AppConf.env == 'test') {
            // user data/roles
            data = {
                subdomain: {
                    brand: "",
                    created_at: new Date(),
                    created_by_user_id: null,
                    deleted_at: null,
                    id: 22,
                    logo_content_type: "image/png",
                    logo_file_name: "360_lexington_logo.png",
                    // logo_file_name: "http://www.lorempixel.com/97/67",
                    logo_file_size: 6482,
                    logo_updated_at: new Date(),
                    name: "360lex",
                    options: 0,
                    position: null,
                    subdomainable_id: 1312,
                    subdomainable_type: "Building",
                    updated_at: new Date(),
                    updated_by_user_id: null
                },
                host_name: '10.0.2.2 → VAGRANT-UBUNTU-TRUSTY-64',
                app_env: 'development',
                release_version: 'V2.0.0-EDGE'
            };
            success(data);
        } else if (AppConf.env == 'rest') {

        }
    },
    fetchUserData: function(options, success, error) {
        if (AppConf.env == 'test') {
            // user data/roles
            console.log(options.user_id);
            data = {
                company: "BI",
                contact_id: 154608,
                email: "paul.lee@buildingintelligence.com",
                first_name: "Paul",
                id: 5512,
                last_name: "Lee",
                login: "paul.lee",
                name: "Paul Lee",
                phone_number: "",
                state: "active",
                title: "",
                // USER ROLES LIKE THIS FOR NOW
                is_superuser: true,
                is_security: false,
                is_engineer: false,
                is_scheduler: true,
                is_dock_worker: false,
                is_dock_admin: true,
                is_group_admin: false,
                is_building_guard: {
                    check: true,
                    buildings: [{

                    }]
                },
                // RELATED GROUPS, VENDORS, BUILDINGS
                groups: [{
                    admin_approve: false,
                    billing_address_id: null,
                    created_at: new Date(),
                    created_by_user_id: 3,
                    deleted_at: null,
                    description: "",
                    features: 0,
                    hostname: "",
                    id: 99,
                    join: 0,
                    name: "Building Intelligence Inc.",
                    primary_address_id: 17881,
                    primary_contact_id: 3,
                    updated_at: new Date(),
                    updated_by_user_id: 3,
                    version: null
                }],
                vendors: [{
                    id: 4143,
                    name: "Shortpath Account Support"
                }],
                buildings: [{
                    address: "360 Lexington Ave, New York, NY",
                    id: 1312,
                    name: "360 Lexington Avenue"
                }, {
                    address: "360 Lexington Ave, New York, NY",
                    id: 1312,
                    name: "360 Lexington Avenue"
                }, {
                    address: "1617 JFK Boulevard, Philadelphia, PA",
                    id: 2441,
                    name: "One Penn Center"
                }]
            };
            success(data);
        } else if (AppConf.env == 'rest') {

        }
    },
    fetchGroupData: function(options, success, error) {
        if (AppConf.env == 'test') {
            // find by options.group_id
            console.log(options.group_id);
            data = {
                admin_approve: false,
                billing_address_id: null,
                created_at: new Date(),
                created_by_user_id: 3,
                deleted_at: null,
                description: "",
                features: 0,
                hostname: "",
                id: 99,
                join: 0,
                name: "Building Intelligence Inc.",
                primary_address_id: 17881,
                primary_contact_id: 3,
                updated_at: new Date(),
                updated_by_user_id: 3,
                version: null,
                orders: [{
                    notice: "A LOT OF ORDER OBJECTS HERE"
                }]
            };
            success(data);
        } else if (AppConf.env == 'rest') {

        }
    },
    createEvent: function(options, success, error) {
        if (AppConf.env == 'test') {
            console.log(options.event_data);
            success({
                message: "Event successfully created!"
            });
        } else if (AppConf.env == 'rest') {

        }
    }
    // options = {campus_id: ''}
    // fetchControlPoints: function(options, success, error) {
    //     if (AppConf.env == 'rest') {
    //         // http://localhost.com:3000/api2/campuses/5413d03c7469616e76000000/control_points.json
    //         net.get({
    //             url: AppConf.host + 'api2/campuses/' + options.campus_id + '/control_points.json',
    //             success: function(data, textStatus, jqXHR) {
    //                 success(data, textStatus, jqXHR);
    //             },
    //             error: function(data, textStatus) {
    //                 error(data, textStatus);
    //             }
    //         });
    //     }
    // },
};