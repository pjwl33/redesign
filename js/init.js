// var app_initializer = {
//     initialize: function() {
//         this.bindEvents();
//     },
//     bindEvents: function() {
//         document.addEventListener('deviceready', this.onDeviceReady, false);
//     },
//     onDeviceReady: function() {
//         // check version
//         cordova.getAppVersion().then(function(version) {
//             console.log("app_version", version);
//         });
//         // init honeywell scanner
//         navigator.honeywell_scanner_plugin.scan(
//             function(data) {},
//             function(err) {});
//         // init back btn
//         document.addEventListener("backbutton", window.goBack, false);
//     }
// };
// app_initializer.initialize();


window.app = {
    setStore: function(key, obj) {
        var objType = typeof(obj);
        var storeVal = obj;
        if (objType == 'object') {
            storeVal = JSON.stringify(obj);
        }
        localStorage.setItem(key, storeVal);
    },
    getStore: function(key) {
        var storeVal = localStorage.getItem(key);
        var obj = null;
        try {
            obj = JSON.parse(storeVal);
        } catch (e) {
            obj = storeVal;
        }
        return obj;
    },
    removeStore: function(key) {
        localStorage.removeItem(key);
    },
    clearInfo: function() {
        for (var k in localStorage) {
            if (k != 'app_config' && k != 'remember_signin') {
                this.removeStore(k);
            }
        }
    },
    currentUser: function() {
        var self = this;
        var current_user_attr = self.getStore('current_user_attr');
        if (current_user_attr) {
            return current_user_attr;
        }
        return null;
    }
};


window.net = {
    setupAjax: function(opts) {
        opts.beforeSend = function(xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + app.getStore('http_basic_auth_val'));
        };
    },
    get: function(opts) {
        var self = this;
        opts.type = "GET";
        self.setupAjax(opts);
        $.ajax(opts);
    },
    post: function(opts) {
        var self = this;
        opts.type = "POST";
        self.setupAjax(opts);
        $.ajax(opts);
    }
};