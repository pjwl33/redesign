ang.controller('NewEventCtrl', function($scope, $routeParams, $timeout) {

    function getUserData() {
        if (app.currentUser()) {
            $scope.user = app.currentUser();
        } else {
            AppService.fetchUserData({
                user_id: 1234
            }, function(data) {
                console.log(data);
                $scope.user = data;
                $scope.groups = data.groups;
                getGroupData(_.first(data.groups));
                $scope.vendors = data.vendors;
                $scope.buildings = data.buildings;
            }, function(data, errorText) {
                console.log(errorText);
            });
        }
    }

    function getAppData() {
        AppService.fetchAppData({}, function(data) {
            console.log(data);
            $scope.subDomain = data.subdomain;
            $scope.hostName = data.host_name;
            $scope.appEnv = data.app_env;
            $scope.releaseVersion = data.release_version;
            $scope.nowTime = new Date();
        }, function(data, errorText) {
            console.log(errorText);
        });
    }

    function getGroupData(group) {
        AppService.fetchGroupData({
            group_id: group.id
        }, function(data) {
            $scope.currentGroup = data;
        }, function(data, errorText) {
            console.log(errorText);
        });
    }

    getUserData();
    getAppData();

});