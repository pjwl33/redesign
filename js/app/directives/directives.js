ang.directive('mainHeader', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/mainHeader.html'
    };
});

// ang.directive('selectionList', function() {
//     return {
//         restrict: 'E',
//         templateUrl: 'partials/selectionList.html'
//     };
// });

// ang.directive('ngBack', function($window) {
//     return function(scope, element, attrs) {
//         return element.bind('click', function() {
//             return scope.customBack ? scope.customBack() : $window.history.back();
//         });
//     };
// });

// ang.directive('ngSignout', function($window, $ionicPopup) {
//     return function(scope, element, attrs) {
//         return element.bind('click', function() {
//             var confirmSignout = $ionicPopup.show({
//                 title: "Sign Out",
//                 template: "Are you sure you want to sign out?",
//                 buttons: [{
//                     text: '<b>Sign Out<b>',
//                     type: 'button-assertive',
//                     onTap: function(e, element){
//                       app.clearInfo();
//                       $window.location.href = "#/";
//                       $window.location.reload();
//                     }
//                 }, {
//                     text: 'Close',
//                     type: 'button-stable',
//                     onTap: function(e, element){
//                     }
//                 }]
//             });
//         });
//     };
// });

// ang.directive('ngCheckAuth', function($document, $window){
//     return function(scope, element, attrs) {
//         return $document.ready(function() {
//             if (app.currentUser()){
//                 return true;
//             } else {
//                 $window.location.href = "#/";
//             }
//         });
//     };
// });