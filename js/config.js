window.AppConf = function() {
    // default AppConf, QR code scanning will reconfigure this object
    var defaultAppConf = {
        // env: 'rest',
        env: 'test',
        // host: 'https://m.vscworldtradecenter.com/',
        host: 'http://staging1.bi.sv3.us/',
        // mqUrl: 'tcp://m.vscworldtradecenter.com:1883',
        mqUrl: 'tcp://staging1.bi.sv3.us:1883',
        mqUsr: 'mobile',
        mqPwd: 'mobile'
    };
    var appConfig = app.getStore('app_config');
    if(appConfig===null) appConfig={};
    var c = $.extend({}, defaultAppConf, appConfig);
    return c;
}();
